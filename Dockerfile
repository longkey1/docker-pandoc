FROM debian:stretch

# install base tools
RUN apt-get update && apt-get install -y apt-utils netbase

# install pandoc
RUN apt-get install -y pandoc

# install noto-fonts
RUN apt-get install -y fonts-noto
